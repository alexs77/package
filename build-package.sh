#!/bin/sh

name=mytool
version=0.25

mkdir $name-$version
cd $name-$version
mkdir -p opt/me/scripts

cat > opt/me/scripts/hello <<EOF
#!/bin/sh
echo "Hello."
EOF

cd ..
tar -cvJf $name-$version.tar.xz $name-$version
cd $name-$version
dh_make --file ../$name-$version.tar.xz --single --copyright bsd --email a@skwar.me --yes
dpkg-buildpackage -us -uc

dpkg -x mytool_0.25-1_amd64.deb deb-mytool_0.25

# EOF
