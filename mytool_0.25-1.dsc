Format: 3.0 (quilt)
Source: mytool
Binary: mytool
Architecture: any
Version: 0.25-1
Maintainer: Alexander Skwar <a@skwar.me>
Homepage: <insert the upstream URL, if relevant>
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 8.0.0)
Package-List: 
 mytool deb unknown optional
Checksums-Sha1: 
 20be2ccb13288e3b1df291de4b2f943a0d148dc5 260 mytool_0.25.orig.tar.xz
 711e2b4dd63f464746ded8fe59943c50dacff7be 9736 mytool_0.25-1.debian.tar.gz
Checksums-Sha256: 
 618051bcdfd90c02e82dc269c78e5250bd4f4de9a5ce56f5f4d867626368e81f 260 mytool_0.25.orig.tar.xz
 33473a5e08f026f2a003af74eccc761ab91a5af7230293c1ad92e41d5d4e0019 9736 mytool_0.25-1.debian.tar.gz
Files: 
 338f0a9b6c854e67e7725cfe32741f00 260 mytool_0.25.orig.tar.xz
 9fdad263029205064b6bb8be09f005b2 9736 mytool_0.25-1.debian.tar.gz
